# Code Golf - US States problem solved in Python, Haskell

Given each US state (and a federal district) print the corresponding US Postal Service abbreviation. 
https://code.golf/united-states#python


## Method 
The first letter of the abbreviation is always the first letter of the state name.
A two letter hash of the state name used to find the index into the array of second letters of the abbreviation.

## Python details

```c``` is the array of second letters, ALabama, AlasKa, etc.

```
c="LKZRAOTECLNIDLNASYAEDAINSOTEVHJMYCDHKRAICDNXTTAAVIY"
```

```d``` is the hash table. "lb" is hash key for Alabama, "as" is key for Alaska, etc. 

```
d="lbasrzArirCoccDe illeewHhIIlnIIInKKeaanaMlsaMiaopsMsoaNbvesieJciNerCkaOoOkeOaalIrtkheeaTUheVViosgrnoyg"
```

The hash is created from two strings, ```f``` and ```v```, both created by repeating the state name. For 
```v``` characters (space, M, N, V, m, n v, and w) are removed.    

```
	f=s+s+s
	u=s
	for i in range(7):u=u.replace(" MNWmnw"[i],"")
	v=u
	for n in range(5):v=v+u
```

The hash key is the 9th letter of ```f``` and the 15th letter of ```v```. Find the hash key in the table of hash keys.  
```n``` is the index into the table of hash keys. ```n//2``` is the index into ```c``` to get the second letter
of the abbreviation.

```
	for n in range(0,102,2):
		if d[n:n+2]==f[8]+v[15]:break
```

Georgia and Lousisiana have hash collisions, overwrite the hash index to result in an index to "A"

```
	if s[0] in "GL":n=8
```

Print the abbreviation.

```
	print(s[0]+c[n//2])
```

## Possible improvements

* Find an hash that uses shorter strings. This can be helpled if all collisions are of states with 'A' as the second letter, or use the last letter or second letter of the state name.
* Find a hash that doesn't find collisions on odd indexes, then you can replace
	```
	for n in range(0,102,2):
		if d[n:n+2]==f[8]+v[15]:break
	```
with
	```
	n = d.find(f[8]+v[15])
	```

* Hash table is of methods (use second letter, use third etc., use last letter, use 'N'), then order the hashtable, and run-length encode it. 

## Regular expressions

Very compact solutions exist using regular expressions. 
